package net.therap.therapMeal.servlet;

import net.therap.therapMeal.domain.Item;
import net.therap.therapMeal.service.ItemService;
import net.therap.therapMeal.validator.ItemValidator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author rakibul.hasan
 * @since 3/1/20
 */
@WebServlet("/items")
public class ItemServlet extends HttpServlet {

    public void service(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String action = request.getParameter("action");

        if (action.equals("view")) {
            viewItems(request, response);

        } else if (action.equals("add")) {
            addItem(request, response);

        } else if (action.equals("remove")) {
            removeItem(request, response);

        } else if (action.equals("updateCall")) {
            updateItemCall(request, response);

        } else if (action.equals("addCall")) {
            addItemCall(request, response);

        } else {
            updateItem(request, response);
        }
    }

    public void viewItems(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ItemService itemService = new ItemService();
        List<Item> items = itemService.getItems();

        request.setAttribute("items", items);

        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/item.jsp");
        rd.forward(request, response);
    }

    public void addItem(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String itemName = request.getParameter("itemName");

        ItemValidator itemValidator = new ItemValidator();
        String error = itemValidator.check(itemName);

        if (error.equals("Ok")) {
            ItemService itemService = new ItemService();
            itemService.addNewItem(itemName);

            viewItems(request, response);
        }

        request.setAttribute("error", error);
        addItemCall(request, response);
    }

    public void removeItem(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int itemId = Integer.parseInt(request.getParameter("itemId"));

        Item item = new Item();
        item.setId(itemId);

        ItemService itemService = new ItemService();
        itemService.removeItem(item);

        viewItems(request, response);
    }

    public void updateItem(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int itemId = Integer.parseInt(request.getParameter("action"));
        String itemName = request.getParameter("itemName");

        ItemValidator itemValidator = new ItemValidator();
        String error = itemValidator.check(itemName);
        if (error.equals("Ok")) {
            ItemService itemService = new ItemService();
            itemService.updateItem(itemId, itemName);

            viewItems(request, response);
        }

        request.setAttribute("error", error);
        request.setAttribute("itemId", itemId);
        request.setAttribute("itemName", itemName);
        request.setAttribute("action", "update");

        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/updateOrAddItem.jsp");
        rd.forward(request, response);
    }

    public void updateItemCall(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int itemId = Integer.parseInt(request.getParameter("itemId"));
        String itemName = request.getParameter("itemName");

        request.setAttribute("itemId", itemId);
        request.setAttribute("itemName", itemName);
        request.setAttribute("action", "update");

        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/updateOrAddItem.jsp");
        rd.forward(request, response);
    }

    public void addItemCall(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("action", "add");
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/updateOrAddItem.jsp");
        rd.forward(request, response);
    }
}
