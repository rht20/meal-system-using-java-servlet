package net.therap.therapMeal.servlet;

import net.therap.therapMeal.domain.Day;
import net.therap.therapMeal.domain.Item;
import net.therap.therapMeal.domain.MealType;
import net.therap.therapMeal.domain.Menu;
import net.therap.therapMeal.service.ItemService;
import net.therap.therapMeal.service.MealTypeService;
import net.therap.therapMeal.service.MenuService;
import net.therap.therapMeal.validator.MenuValidator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * @author rakibul.hasan
 * @since 3/1/20
 */
@WebServlet("/menu")
public class MenuServlet extends HttpServlet {

    public void service(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String action = request.getParameter("action");

        if (action.equals("view")) {
            viewMenu(request, response);

        } else if (action.equals("update")){
            updateMenu(request, response);

        } else if (action.equals("remove")) {
            removeMenu(request, response);

        } else {
            updateCall(request, response);
        }
    }

    public void viewMenu(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        MenuService menuService = new MenuService();
        List<Map> menus = process(menuService.getMenus());
        request.setAttribute("menus", menus);

        MealTypeService mealTypeService = new MealTypeService();
        List<MealType> mealTypes = mealTypeService.getMealTypes();
        request.setAttribute("mealTypes", mealTypes);

        List<Day> days = Arrays.asList(Day.values());
        request.setAttribute("days", days);

        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/menu.jsp");
        rd.forward(request, response);
    }

    public void updateMenu(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int mealTypeId = Integer.parseInt(request.getParameter("mealTypeId"));
        String day = request.getParameter("day");
        String[] itemIds = request.getParameterValues("itemId");

        Map taken = new HashMap<>();
        if (itemIds != null) {
            for (String itemId : itemIds) {
                int id = Integer.parseInt(itemId);
                taken.put(id, true);
            }
        }

        MenuService menuService = new MenuService();
        Menu menu = menuService.getMenu(mealTypeId, day);

        Map inMenu = new HashMap<>();
        List<Integer> removeList = new ArrayList<>();
        if (menu.getItemList() != null) {
            for (Item item : menu.getItemList()) {
                if (taken.get(item.getId()) == null) {
                    removeList.add(item.getId());
                }
                inMenu.put(item.getId(), true);
            }
        }
        for (int x : removeList) {
            menuService.removeItemFromMenu(mealTypeId, day, x);
        }

        if (itemIds != null) {
            for (String itemId : itemIds) {
                int id = Integer.parseInt(itemId);
                if (inMenu.get(id) == null) {
                    menuService.addItemToMenu(mealTypeId, day, id);
                }
            }
        }

        viewMenu(request, response);
    }

    public void removeMenu(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int menuId = Integer.parseInt(request.getParameter("menuId"));

        MenuService menuService = new MenuService();
        menuService.removeMenu(menuId);

        viewMenu(request, response);
    }

    public void updateCall(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String day = request.getParameter("day");
        String option = request.getParameter("mealType");

        MenuValidator menuValidator = new MenuValidator();
        String error = menuValidator.check(day, option);

        if (!error.equals("Ok")) {
            request.setAttribute("error", error);
            viewMenu(request, response);
        }

        String mealTypeName = option.split(":")[1];
        int mealTypeId = Integer.parseInt(option.split(":")[0]);

        MenuService menuService = new MenuService();
        Menu menu = menuService.getMenu(mealTypeId, day);

        ItemService itemService = new ItemService();
        List<Item> items = itemService.getItems();

        List<Map> list = new ArrayList<>();
        Map mark = new HashMap<>();
        for (Item item : menu.getItemList()) {
            Map map = new HashMap<>();
            map.put("itemId", item.getId());
            map.put("itemName", item.getName());
            map.put("status", true);
            list.add(map);
            mark.put(item.getId(), true);
        }
        for (Item item : items) {
            if (mark.get(item.getId()) == null) {
                Map map = new HashMap<>();
                map.put("itemId", item.getId());
                map.put("itemName", item.getName());
                map.put("status", false);
                list.add(map);
            }
        }

        request.setAttribute("mealTypeName", mealTypeName);
        request.setAttribute("mealTypeId", mealTypeId);
        request.setAttribute("day", day);
        request.setAttribute("items", list);

        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/updateMenu.jsp");
        rd.forward(request, response);
    }

    public List<Map> process(List<Menu> menus) {
        List<Map> list = new ArrayList<>();
        for (Menu menu : menus) {
            Map map = new HashMap<>();

            map.put("menuId", menu.getId());
            map.put("day", menu.getDay());
            map.put("mealType", menu.getMealType().getName());

            String items = "";
            boolean flag = false;
            for (Item item : menu.getItemList()) {
                if (flag) {
                    items += ", ";
                }
                items += item.getName();
                flag = true;
            }
            if (items.equals("")) {
                continue;
            }
            map.put("items", items);

            list.add(map);
        }

        return list;
    }
}
