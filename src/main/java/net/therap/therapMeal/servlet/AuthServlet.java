package net.therap.therapMeal.servlet;

import net.therap.therapMeal.validator.LoginValidator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author rakibul.hasan
 * @since 3/2/20
 */
@WebServlet("/")
public class AuthServlet extends HttpServlet {

    public void service(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String action = request.getParameter("action");

        if (action == null) {
            HttpSession session = request.getSession();

            if (session.getAttribute("uname") != null) {
                response.sendRedirect("/home");
                return;
            }

            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/index.jsp");
            rd.forward(request, response);
        }
        else if (action.equals("login")) {
            login(request, response);

        } else if (action.equals("logout")) {
            logout(request, response);
        }
    }

    public void login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uname = request.getParameter("uname");
        String password = request.getParameter("password");

        LoginValidator loginValidator = new LoginValidator();
        String error = loginValidator.check(uname, password);

        if (error.equals("Ok")) {
            HttpSession session = request.getSession();
            session.setAttribute("uname", uname);

            response.sendRedirect("/home");
        } else {
            request.setAttribute("error", error);

            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/index.jsp");
            rd.forward(request, response);
        }
    }

    public void logout(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        session.removeAttribute("uname");
        session.invalidate();

        response.sendRedirect("/");
    }
}
