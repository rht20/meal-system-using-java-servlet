package net.therap.therapMeal.servlet;

import net.therap.therapMeal.domain.MealType;
import net.therap.therapMeal.service.MealTypeService;
import net.therap.therapMeal.validator.MealTypeValidator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author rakibul.hasan
 * @since 3/1/20
 */
@WebServlet("/mealTypes")
public class MealTypeServlet extends HttpServlet {

    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");

        if (action.equals("view")) {
            viewMealTypes(request, response);

        } else if (action.equals("add")) {
            addMealType(request, response);

        } else if (action.equals("remove")) {
            removeMealType(request, response);

        } else if (action.equals("updateCall")) {
            updateMealTypeCall(request, response);

        } else if (action.equals("addCall")) {
            addMealTypeCall(request, response);

        } else {
            updateMealType(request, response);
        }
    }

    public void viewMealTypes(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        MealTypeService mealTypeService = new MealTypeService();
        List<MealType> mealTypes = mealTypeService.getMealTypes();

        request.setAttribute("mealTypes", mealTypes);

        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/mealType.jsp");
        rd.forward(request, response);
    }

    public void addMealType(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String mealTypeName = request.getParameter("mealTypeName");

        MealTypeValidator mealTypeValidator = new MealTypeValidator();
        String error = mealTypeValidator.check(mealTypeName);
        if (error.equals("Ok")) {
            MealTypeService mealTypeService = new MealTypeService();
            mealTypeService.addMealType(mealTypeName);

            viewMealTypes(request, response);
        }

        request.setAttribute("error", error);
        addMealTypeCall(request, response);
    }

    public void removeMealType(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int mealTypeId = Integer.parseInt(request.getParameter("mealTypeId"));

        MealType mealType = new MealType();
        mealType.setId(mealTypeId);

        MealTypeService mealTypeService = new MealTypeService();
        mealTypeService.removeMealType(mealTypeId);

        viewMealTypes(request, response);
    }

    public void updateMealType(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int mealTypeId = Integer.parseInt(request.getParameter("action"));
        String mealTypeName = request.getParameter("mealTypeName");

        MealTypeValidator mealTypeValidator = new MealTypeValidator();
        String error = mealTypeValidator.check(mealTypeName);

        if (error.equals("Ok")) {
            MealTypeService mealTypeService = new MealTypeService();
            mealTypeService.updateMealType(mealTypeId, mealTypeName);

            viewMealTypes(request, response);
        }

        request.setAttribute("error", error);
        request.setAttribute("mealTypeId", mealTypeId);
        request.setAttribute("mealTypeName", mealTypeName);
        request.setAttribute("action", "update");

        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/updateOrAddMealType.jsp");
        rd.forward(request, response);
    }

    public void updateMealTypeCall(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int mealTypeId = Integer.parseInt(request.getParameter("mealTypeId"));
        String mealTypeName = request.getParameter("mealTypeName");

        request.setAttribute("mealTypeId", mealTypeId);
        request.setAttribute("mealTypeName", mealTypeName);
        request.setAttribute("action", "update");

        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/updateOrAddMealType.jsp");
        rd.forward(request, response);
    }

    public void addMealTypeCall(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("action", "add");
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/updateOrAddMealType.jsp");
        rd.forward(request, response);
    }
}
