package net.therap.therapMeal.validator;

import net.therap.therapMeal.domain.MealType;
import net.therap.therapMeal.service.MealTypeService;

import java.util.List;

/**
 * @author rakibul.hasan
 * @since 3/2/20
 */
public class MealTypeValidator {

    public String check(String mealTypeName) {

        if (mealTypeName.equals("")) {
            return "Meal Type name can't be empty.";
        }

        MealTypeService mealTypeService = new MealTypeService();
        List<MealType> mealTypes = mealTypeService.getMealTypes();

        for (MealType mealType : mealTypes) {
            if (mealTypeName.equals(mealType.getName())) {
                return "Meal Type already exist.";
            }
        }

        return "Ok";
    }
}
