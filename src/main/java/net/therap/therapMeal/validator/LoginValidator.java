package net.therap.therapMeal.validator;

import net.therap.therapMeal.service.LoginService;

/**
 * @author rakibul.hasan
 * @since 3/2/20
 */
public class LoginValidator {

    public String check(String uname, String password) {

        if (uname.equals("") || password.matches("")) {
            return "Please enter your User name and password.";
        }

        LoginService loginService = new LoginService();
        if (loginService.check(uname, password)) {
            return "Ok";
        }

        return "Invalid user name or password.";
    }
}
