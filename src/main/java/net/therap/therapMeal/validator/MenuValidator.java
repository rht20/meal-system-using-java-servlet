package net.therap.therapMeal.validator;

/**
 * @author rakibul.hasan
 * @since 3/2/20
 */
public class MenuValidator {

    public String check(String day, String mealType) {
        if (day.equals("") || mealType.equals("")) {
            return "Please select day and meal type.";
        }

        return "Ok";
    }
}
