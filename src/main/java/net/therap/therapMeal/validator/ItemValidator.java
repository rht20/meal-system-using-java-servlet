package net.therap.therapMeal.validator;

import net.therap.therapMeal.domain.Item;
import net.therap.therapMeal.service.ItemService;

import java.util.List;

/**
 * @author rakibul.hasan
 * @since 3/2/20
 */
public class ItemValidator {

    public String check(String itemName) {
        if (itemName.equals("")) {
            return "Item name can't be empty.";
        }

        ItemService itemService = new ItemService();
        List<Item> items = itemService.getItems();

        for (Item item : items) {
            if (itemName.equals(item.getName())) {
                return "Item already exist.";
            }
        }

        return "Ok";
    }
}
