package net.therap.therapMeal.service;

import net.therap.therapMeal.dao.MealTypeDao;
import net.therap.therapMeal.dao.MenuDao;
import net.therap.therapMeal.domain.MealType;

import java.util.List;

/**
 * @author rakibul.hasan
 * @since 2/11/20
 */
public class MealTypeService {

    private MealTypeDao mealTypeDao;
    private MenuDao menuDao;

    public MealTypeService() {
        this.mealTypeDao = new MealTypeDao();
        this.menuDao = new MenuDao();
    }

    public List<MealType> getMealTypes() {
        List<MealType> mealTypes = mealTypeDao.getMealTypes();
        return mealTypes;
    }

    public void addMealType(String typeName) {
        MealType mealType = new MealType();
        mealType.setName(typeName);
        mealTypeDao.insertMealType(mealType);
    }

    public void removeMealType(int mealTypeId) {
        List<MealType> mealTypes = getMealTypes();

        for (MealType mealType : mealTypes) {
            if (mealType.getId() == mealTypeId) {
                menuDao.removeByMealType(mealType);
                mealTypeDao.removeMealType(mealType);
                break;
            }
        }
    }

    public void updateMealType(int mealTypeId, String mealTypeName) {
        MealType mealType1 = new MealType();
        mealType1.setId(mealTypeId);

        MealType mealType2 = new MealType();
        mealType2.setName(mealTypeName);

        mealTypeDao.updateMealType(mealType1, mealType2);
    }
}
