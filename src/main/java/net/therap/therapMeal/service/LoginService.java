package net.therap.therapMeal.service;

import net.therap.therapMeal.domain.User;
import net.therap.therapMeal.helper.EntityManagerHelper;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 * @author rakibul.hasan
 * @since 3/2/20
 */
public class LoginService {

    private EntityManager em;

    public LoginService() {
        this.em = EntityManagerHelper.getEntityManager();
    }

    public boolean check(String uname, String password) {
        TypedQuery<User> query = em.createQuery("SELECT U FROM User AS U WHERE U.uname = :uname AND U.password = :password", User.class)
                .setParameter("uname", uname)
                .setParameter("password", password);

        return query.getResultList().isEmpty() ? false : true;
    }
}
