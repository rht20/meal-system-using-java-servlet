<%--
  Created by IntelliJ IDEA.
  User: rht_20
  Date: 2/26/20
  Time: 9:43 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Menu | Meal System</title>

    <style>
        .center {
            margin: auto;
            width: 40%;
            padding-top: 100px;
        }

        table, td, th {
            border: 1px solid black;
        }

        select {
            width: 100%;
            text-align: center;
            font-size: 18px;
            padding: 9px;
        }

        #table-collapse {
            border-collapse: collapse;
        }

        .button {
            background-color: #4CAF50;
            border: 1px solid green;
            color: white;
            text-align: center;
            font-size: 18px;
            text-decoration: none;
            padding: 10px 24px;
            cursor: pointer;
            display: block;
        }

        .button:not(:last-child) {
            border-bottom: none;
        }

        .button:hover {
            background-color: #3e8e41;
        }

        .btn-group button {
            background-color: #4CAF50;
            border: 1px solid green;
            color: white;
            padding: 10px 24px;
            cursor: pointer;
            width: 100%;
            display: block;
            font-size: 18px;
        }

        .btn-group button:not(:last-child) {
            border-bottom: none;
        }

        .btn-group button:hover {
            background-color: #3e8e41;
        }

        .remove-button {
            background-color: firebrick;
            border: 1px darkred;
        }

        .remove-button:hover {
            background-color: darkred;
        }
    </style>
</head>
<body>
    <div class="center">
        <h1 style="width: 100%; text-align: center">Weekly Meal Menu</h1>
        <table style="width: 100%" class="btn-group" id="table-collapse">
            <tr style="width: 100%;">
                <td style="width: 20%; text-align: center">
                    <h3>Day</h3>
                </td>
                <td style="width: 20%; text-align: center">
                    <h3>Meal Type</h3>
                </td>
                <td style="width: 60%; text-align: center">
                    <h3>Items</h3>
                </td>
                <td style="width: 20%; text-align: center">
                    <h3>Remove</h3>
                </td>
            </tr>

            <c:forEach items="${menus}" var="menu">
                <tr>
                    <td style="text-align: center">${menu['day']}</td>
                    <td style="text-align: center">${menu['mealType']}</td>
                    <td style="text-align: center">${menu['items']}</td>
                    <td style="text-align: center">
                        <a href="/menu?action=remove&menuId=${menu['menuId']}" class="button remove-button">Remove</a>
                    </td>
                </tr>
            </c:forEach>
        </table>

        <br/>
        <br/>
        <br/>
        <br/>

        <h2 style="width: 100%; text-align: center">Select day and meal type to update menu</h2>
        <table style="width: 100%" class="btn-group" id="table-collapse">
            <tr>
                <p style="color: indianred">${error}</p>

                <form action="menu" method="post">
                    <td style="width: 40%;">
                        <select id="day" name="day">
                            <option value="">Select Day</option>

                            <c:forEach items="${days}" var="item">
                                <option value="${item}">${item}</option>
                            </c:forEach>
                        </select>
                    </td>

                    <td style="width: 40%;">
                        <select id="mealType" name="mealType">
                            <option value="">Select Meal Type</option>

                            <c:forEach items="${mealTypes}" var="item">
                                <option value="${item.id}:${item.name}">${item.name}</option>
                            </c:forEach>
                        </select>
                    </td>

                    <td style="width: 20%;">
                        <button id="action" name="action" value="updateCall">Submit</button>
                    </td>

                    <br/>
                    <br/>
                </form>
            </tr>
        </table>

        <br/>
        <br/>
        <br/>
        <br/>

        <a href="/home" class="button">Home</a>
    </div>
</body>
</html>
