<!--

  Created by IntelliJ IDEA.
  User: rakibul.hasan
  Date: 3/2/20
  Time: 4:56 PM
  To change this template use File | Settings | File Templates.
-->
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
  <head>
    <title>Home | Meal System</title>

      <style>
          .center {
              margin: auto;
              width: 20%;
              padding-top: 100px;
          }

          .button {
              background-color: #4CAF50;
              border: 1px solid green;
              color: white;
              text-align: center;
              font-size: 18px;
              text-decoration: none;
              padding: 10px 24px;
              cursor: pointer;
              width: 100%;
              display: block;
          }

          .button:not(:last-child) {
              border-bottom: none;
          }

          .button:hover {
              background-color: #3e8e41;
          }
      </style>
</head>
  <body>
      <div class="center">
          <h1 style="width: 100%; text-align: center; padding-left: 7%;">Meal System</h1>

          <a href="/items?action=view" class="button">Items</a>
          <br/>
          <a href="/mealTypes?action=view" class="button">Meal Types</a>
          <br/>
          <a href="/menu?action=view" class="button">Menu</a>
          <br/>
          <a href="/auth?action=logout" class="button">Logout</a>
      </div>
  </body>
</html>
