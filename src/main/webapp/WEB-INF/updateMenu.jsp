<%--
  Created by IntelliJ IDEA.
  User: rakibul.hasan
  Date: 3/1/20
  Time: 2:46 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title></title>

    <style>
        .center {
            margin: auto;
            width: 20%;
            padding-top: 100px;
        }

        table, td, th {
            border: 1px solid black;
        }

        #table-collapse {
            border-collapse: collapse;
        }

        .btn-group button {
            background-color: #4CAF50;
            border: 1px solid green;
            color: white;
            padding: 10px 24px;
            cursor: pointer;
            width: 100%;
            display: block;
            font-size: 18px;
        }

        .btn-group button:not(:last-child) {
            border-bottom: none;
        }

        .btn-group button:hover {
            background-color: #3e8e41;
        }
    </style>
</head>
<body>
    <div class="center">
        <h2 style="width: 100%; text-align: center">${day}, ${mealTypeName}</h2>
        <br/>

        <form action="menu" method="post" class="btn-group">
            <textarea id="mealTypeId" name="mealTypeId" hidden="hidden">${mealTypeId}</textarea>
            <textarea id="day" name="day" hidden="hidden">${day}</textarea>

            <c:forEach items="${items}" var="map">
                <c:if test="${map['status'] == true}">
                    <input type="checkbox" id="itemId" name="itemId" value="${map['itemId']}" checked>
                </c:if>
                <c:if test="${map['status'] == false}">
                    <input type="checkbox" id="itemId" name="itemId" value="${map['itemId']}">
                </c:if>

                <label style="font-size: 20px;">${map['itemName']}</label>
                <br/>
            </c:forEach>

            <br/>
            <br/>

            <table style="width: 100%;" id="table-collapse">
                <tr>
                    <td style="width: 50%;">
                        <button id="action" name="action" value="view">Cancel</button>
                    </td>
                    <td style="width: 50%;">
                        <button id="action" name="action" value="update">Update</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</body>
</html>
