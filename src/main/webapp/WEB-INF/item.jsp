<%--
  Created by IntelliJ IDEA.
  User: rht_20
  Date: 2/26/20
  Time: 9:35 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Items | Meal System</title>

    <style>
        .center {
            margin: auto;
            width: 30%;
            padding-top: 100px;
        }

        table, td, th {
            border: 1px solid black;
            text-align: center;
        }

        #table-collapse {
            border-collapse: collapse;
        }

        .button {
            background-color: #4CAF50;
            border: 1px solid green;
            color: white;
            text-align: center;
            font-size: 18px;
            text-decoration: none;
            padding: 10px 24px;
            cursor: pointer;
            display: block;
        }

        .button:not(:last-child) {
            border-bottom: none;
        }

        .button:hover {
            background-color: #3e8e41;
        }

        .remove-button {
            background-color: firebrick;
            border: 1px darkred;
        }

        .remove-button:hover {
            background-color: darkred;
        }
    </style>
</head>
<body>
    <div class="center">
        <h1 style="width: 100%; text-align: center">Available Items</h1>

        <table style="width: 100%;" id="table-collapse">
            <c:forEach items="${items}" var="item">
                <tr style="width: 100%;">
                    <td style="width: 34%;">
                        <h2>${item.name}</h2>
                    </td>
                    <td style="width: 33%;">
                        <a href="/items?action=updateCall&itemId=${item.id}&itemName=${item.name}" class="button">Update</a>
                    </td>
                    <td style="width: 33%;">
                        <a href="/items?action=remove&itemId=${item.id}" class="button remove-button">Remove</a>
                    </td>
                </tr>
            </c:forEach>
        </table>

        <br/>
        <br/>
        <br/>

        <table style="width: 100%;" class="btn-group" id="table-collapse">
            <tr style="border: 1px;">
                <td style="width: 50%; text-align: center;">
                    <a href="/home" class="button">Home</a>
                </td>
                <td style="width: 50%; text-align: center">
                    <a href="/items?action=addCall" class="button">Add New Item</a>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
