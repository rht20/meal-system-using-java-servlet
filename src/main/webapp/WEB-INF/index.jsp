<%--
  Created by IntelliJ IDEA.
  User: rakibul.hasan
  Date: 2/26/20
  Time: 2:42 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Meal System</title>

    <style>
        .center {
            margin: auto;
            width: 20%;
            padding-top: 100px;
        }

        table, td, th {
            border: 1px solid black;
            text-align: center;
            font-size: 20px;
        }

        #table-collapse {
            border-collapse: collapse;
        }

        .button {
            background-color: #4CAF50;
            border: 1px solid green;
            color: white;
            text-align: center;
            font-size: 18px;
            text-decoration: none;
            padding: 10px 24px;
            cursor: pointer;
            display: block;
            width: 100%;
        }

        .button:not(:last-child) {
            border-bottom: none;
        }

        .button:hover {
            background-color: #3e8e41;
        }
    </style>
</head>
<body>
<div class="center">
    <form action="/" method="post">
        <p style="color: indianred">${error}</p>

        <table style="width: 100%;" id="table-collapse">
            <tr style="width: 100%;">
                <td style="width: 40%;">User Name</td>
                <td style="width: 60%;">
                    <input type="text" id="uname" name="uname" style="width: 100%;">
                </td>
            </tr>
            <tr>
                <td style="width: 40%;">Password</td>
                <td style="width: 60%;">
                    <input type="password" id="password" name="password" style="width: 100%;">
                </td>
            </tr>
        </table>

        <br/>
        <br/>

        <button class="button" id="action" name="action" value="login">Login</button>
    </form>
</div>
</body>
</html>
